package be.vgqd.vrjam.game.chogath.scene;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.GregorianCalendar;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ButtonBuilder;
import javafx.scene.image.Image;
import javafx.scene.image.ImageViewBuilder;
import be.vgqd.vrjam.game.chogath.Game;
import be.vgqd.vrjam.game.chogath.data.Paths;
import be.vgqd.vrjam.game.chogath.nodes.link.LinkNode;
import be.vgqd.vrjam.game.chogath.oculus.CameraDisplayData;
import be.vgqd.vrjam.game.chogath.oculus.OculusControllerData;
import be.vgqd.vrjam.game.chogath.oculus.OculusScene;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;
import be.vgqd.vrjam.game.chogath.oculus.StereoNodeBuilder;

public class HomeScene extends OculusScene {

	private ButtonBuilder< ?> buttonBuilder = ButtonBuilder.create().prefWidth(100)
			.prefHeight(25);
	private Image background;
	private StereoNode<Node> credit;
	private boolean creditVisibility;
	public HomeScene(Group parent) {
		super(parent);

		addImageBackGround();
		addPlayButton();
		addCreditButton();
		addExitButton();
		

		addCreditPanel();
addLink();
	}

	private void addLink() {
		Image img=null;
		try {
			img = new Image(new FileInputStream(new File(Paths.IMAGE_PATH+"\\component\\logo_beerWare.png")	));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String url ="https://www.paypal.com/be/cgi-bin/webscr?cmd=_flow&SESSION=k7l0Lk8Z11iarqkHqL8B6bwOx02vjNGRO4mvM8MT2LdC5UFgCkCBtFc6IfK&dispatch=5885d80a13c0db1f8e263663d3faee8d48a116ba977951b3435308b8c4dd4ef1" ;
		StereoNode<LinkNode> sn = StereoNodeBuilder.getOculusStereoNode(new LinkNode(url, img),new LinkNode(url, img), -200, 80, 0);		
		this.add(sn);
	}

	private void addCreditPanel() {
		try {
			background = new Image(new FileInputStream(new File(
					Paths.APP_PATH + "\\data\\image\\VgQDLogo.jpg")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		ImageViewBuilder<?>  imgViewBuild = ImageViewBuilder.create()
				.image(background).fitHeight(CameraDisplayData.SCREENHEIGHT/6).fitWidth(CameraDisplayData.SCREENWIDTH/6).visible(false);

		credit = add(imgViewBuild.build(), imgViewBuild.build(), -CameraDisplayData.SCREENHEIGHT/12,-CameraDisplayData.SCREENHEIGHT/12);
		
	}
	
	private void addCreditButton() {
		buttonBuilder.text("X - Credit");
		buttonBuilder.onAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle( ActionEvent event) {
				creditVisibility = !creditVisibility;
				credit.setVisible(creditVisibility);
			}
		});
		this.add(buttonBuilder.build(), buttonBuilder.build(),
				-CameraDisplayData.SCREENWIDTH / 8, 30);
	}

	private void addExitButton() {
		buttonBuilder.text("A - Exit");
		buttonBuilder.onAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle( ActionEvent event) {
				System.out.println("Exit ...");
				System.exit(0);
			}
		});
		this.add(buttonBuilder.build(), buttonBuilder.build(),
				-CameraDisplayData.SCREENWIDTH / 8, 60);
	}

	private void addImageBackGround() {
		try {
			background = new Image(new FileInputStream(new File(
					Paths.APP_PATH + "\\data\\image\\indexBg.jpg")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		ImageViewBuilder<?> imgViewBuild = ImageViewBuilder.create()
				.image(background);
		add(imgViewBuild.build(), imgViewBuild.build(), -background.getWidth()/2,-background.getHeight()/2);
	}

//	private void addOptionButton() {
//		buttonBuilder.text("Option");
//		buttonBuilder.onAction(new EventHandler<ActionEvent>() {
//
//			@Override
//			public void handle(ActionEvent event) {
//				optionVisibility = !optionVisibility;
//				option.setVisible(optionVisibility);
//
//			}
//		});
//		this.add(buttonBuilder.build(), buttonBuilder.build(),
//				-StereoCamera.SCREENWIDTH / 8, 0);
//	}

	private void addPlayButton() {
		buttonBuilder.text("Y - Play");
		buttonBuilder.onAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				if(! OculusControllerData.isConnected())
				{
					Game.getMode().displayDialMessage("The Oculus Rift is not connected. Please relaunch the application and check that the Oculus rift is correctly connected or is not used by an other application");
					
				}
				else {	Game.getInstance().start();
			}

			}
		});
		this.add(buttonBuilder.build(), buttonBuilder.build(),
				-CameraDisplayData.SCREENWIDTH / 8, 0);
	}

	private long lastTime ;
	public void displayCredit() {
		displayCredit(!creditVisibility);}
	public void displayCredit(boolean on) {
		long now=GregorianCalendar.getInstance().getTimeInMillis();
		if(now-lastTime>500)
		{
			lastTime=now;
		creditVisibility=on;
		credit.setVisible(on);
		}
	}

	
}
