package be.vgqd.vrjam.game.chogath.oculus;

import javafx.geometry.Point2D;
import javafx.scene.Node;

/**
 * @author Eloi
 * 
 * @param <E>
 */
public class StereoNode<E extends Node> {
	private final Point2D leftInitialPoint;
	private final Point2D rightInitialPoint;
	private final E left, right;

	public StereoNode(E left, double xLeft, double yLeft, E right,
			double xRight, double yRight) {
		if (left == null || right == null)
			throw new NullPointerException();
		this.left = left;
		this.right = right;
		leftInitialPoint = new Point2D(xLeft, yLeft);
		rightInitialPoint = new Point2D(xRight, yRight);

	}

	public final double getLayoutXLeft() {
		return left.getLayoutX();
	}

	public final double getLayoutYLeft() {
		return left.getLayoutY();
	}

	public final void setLayoutXLeft(double arg0) {
		left.setLayoutX(arg0 + leftInitialPoint.getX());
	}

	public final void setLayoutYLeft(double arg0) {
		left.setLayoutY(arg0 + leftInitialPoint.getY());
	}

	public final double getLayoutXRight() {
		return right.getLayoutX();
	}

	public final double getLayoutYRight() {
		return right.getLayoutY();
	}

	public final void setLayoutXRight(double arg0) {
		right.setLayoutX(arg0 + rightInitialPoint.getX());
	}

	public final void setLayoutYRight(double arg0) {
		right.setLayoutY(arg0 + rightInitialPoint.getY());
	}

	public final void setLayout(double x, double y) {
		setLayoutXRight(x);
		setLayoutXLeft(x);
		setLayoutYRight(y);
		setLayoutYLeft(y);

	}

	public final double getLayoutX() {
		return (getLayoutXRight() + getLayoutXLeft()) / 2.0;

	}

	public final double getLayoutY() {
		return (getLayoutYRight() + getLayoutYLeft()) / 2.0;

	}

	public Point2D getLeftInitialPoint() {
		return leftInitialPoint;
	}

	public Point2D getRightInitialPoint() {
		return rightInitialPoint;
	}

	public E getLeft() {
		return left;
	}

	public E getRight() {
		return right;
	}

	public final void setVisible(boolean value) {
		left.setVisible(value);
		right.setVisible(value);
	}

	public void setOpacity(double value) {
		left.setOpacity(value);
		right.setOpacity(value);
	}

	public void setRotation(double angle) {
		left.setRotate(angle);
		right.setRotate(angle);

	}

	public void setScaleX(double value) {
		left.setScaleX(value);
		right.setScaleX(value);

	}

	public void setScaleY(double value) {
		left.setScaleY(value);
		right.setScaleY(value);

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StereoNode [x=" + getLayoutX() + ", y=" + getLayoutY()
				+ ", " + "left=");
		builder.append(left);
		builder.append(", right=");
		builder.append(right);
		builder.append("]");
		return builder.toString();
	}

	public double getRotation() {
		return left.getRotate();
	}

}
