package be.vgqd.vrjam.game.chogath.oculus;

import javafx.scene.Node;
import javafx.scene.layout.Pane;


public class StereoNodeBuilder{

	//Je sais que je devrai pas mais bon, tempis pour cette fois-ci.
	//Il me faut �tudier comment ne plus tomber sur ce genre de warning
	@SuppressWarnings("unchecked")
	public final static <E extends Node>  StereoNode<E> getOculusStereoNode (E left, E right, double x, double  y, double deepness)
	{
		if (left==null)left= (E) new Pane();
		if (right==null)right=(E) new Pane();
		StereoNode<E> sn = null;
		double lx =CameraDisplayData.SCREENWIDTH/4+CameraDisplayData.getAdjustment()/2-deepness;
		double rx =CameraDisplayData.SCREENWIDTH/4+-CameraDisplayData.getAdjustment()/2+deepness;
		double ly =CameraDisplayData.SCREENHEIGHT/2;
		double ry =CameraDisplayData.SCREENHEIGHT/2;
		sn=new StereoNode<E>(left,lx,ly,right,rx,ry);
		sn.setLayout(x, y);
		return sn;
	}
	@SuppressWarnings("unchecked")
	public final  static <E extends Node>  StereoNode<E> getStereoNode (E left, E right, double x, double  y)
	{
		if (left==null)left=(E) new Pane();
		if (right==null)right=(E) new Pane();
		StereoNode<E> sn = null;
		sn=new StereoNode<E>(left,0,0,right,0,0);
		sn.setLayout(x, y);
		return sn;
	}
	@SuppressWarnings("unchecked")
	public final  static <E extends Node>  StereoNode<E> getStereoNode (E left, E right, double x, double  y,double leftCamX,double leftCamY,double rightCamX,double rightCamY)
	{
		if (left==null)left=(E) new Pane();
		if (right==null)right=(E) new Pane();
		StereoNode<E> sn = null;
		sn=new StereoNode<E>(left,leftCamX,leftCamY,right,rightCamX,rightCamY);
		sn.setLayout(x, y);
		return sn;
	}
	
}
