package be.vgqd.vrjam.game.chogath.oculus;

public class RYP {
	double roll, yaw, pitch;
	
	public RYP (double roll, double yaw,double pitch)
	{
		this.roll = roll;
		this.yaw = yaw;
		this.pitch= pitch;
		
	}
	public void setRYP (double roll, double yaw,double pitch)
	{
	setPitch(pitch);
	setRoll(roll);
	setYaw(yaw);
		
	}

	public double getRoll() {
		return roll;
	}

	public void setRoll(double roll) {
		this.roll = roll;
	}

	public double getYaw() {
		return yaw;
	}

	public void setYaw(double yaw) {
		this.yaw = yaw;
	}

	public double getPitch() {
		return pitch;
	}

	public void setPitch(double pitch) {
		this.pitch = pitch;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RYP [roll=");
		builder.append(roll);
		builder.append(", yaw=");
		builder.append(yaw);
		builder.append(", pitch=");
		builder.append(pitch);
		builder.append("]");
		return builder.toString();
	}


	
}
