package be.vgqd.vrjam.game.chogath.oculus;

import de.fruitfly.ovr.OculusRift;

public class OculusControllerData {
	private static final OculusRift oculus;
	private static  OculusControllerData INSTANCE;
	static
	{
		
		oculus = new OculusRift();
		oculus.init();
		oculus.poll();
		
		
		
	}
	public static OculusRift getOculusInstance() {
		return oculus;
	}
	public static OculusControllerData getInstance() {

		if(INSTANCE==null)INSTANCE = new OculusControllerData();
		return INSTANCE;
	}
	public static boolean isConnected() {
		return oculus.isInitialized();
	}
	

	private static final RYP lastRefresh=new RYP(0, 0, 0);
	private static final RYP frontView=new RYP(0, 0, 0);
	private   RYP screenTopLeft;
	private   RYP screenTopRight;
	private   RYP screenBottomLeft;
	private   RYP screenBottomRight;
	
	public  boolean isScreenCornersRYPRecorded()
	{
		if( screenBottomLeft==null ||screenTopRight==null ||screenBottomLeft==null ||screenBottomRight==null ) return false;
		return true;
	}
	public static void saveCurrentRYPAsFrontView() {
		if( oculus!=null && oculus.isInitialized())
		{
			oculus.poll();
			frontView.setRYP(oculus.getRoll(), oculus.getYaw(),oculus.getPitch());
			
			
		}
		
	}public static void saveLastPositionRYP() {
		if( oculus!=null && oculus.isInitialized())
		{
			oculus.poll();
			lastRefresh.setRYP(oculus.getRoll(), oculus.getYaw(),oculus.getPitch());
			
			
		}
		
	}
	public static void saveLastPositionRYP(double roll, double yaw, double pitch) {
		
			lastRefresh.setRYP(roll,yaw, pitch);
			
			
		
		
	}
	public RYP getScreenTopLeft() {
		return screenTopLeft;
	}
	public void setScreenTopLeft(RYP screenTopLeft) {
		this.screenTopLeft = screenTopLeft;
	}
	public RYP getScreenTopRight() {
		return screenTopRight;
	}
	public void setScreenTopRight(RYP screenTopRight) {
		this.screenTopRight = screenTopRight;
	}
	public RYP getScreenBottomLeft() {
		return screenBottomLeft;
	}
	public void setScreenBottomLeft(RYP screenBottomLeft) {
		this.screenBottomLeft = screenBottomLeft;
	}
	public RYP getScreenBottomRight() {
		return screenBottomRight;
	}
	public void setScreenBottomRight(RYP screenBottomRight) {
		this.screenBottomRight = screenBottomRight;
	}
	public static RYP getLastPositionRYP() {
		return lastRefresh;
	}
	public static RYP getFrontview() {
		return frontView;
	}
	
	
	
}
