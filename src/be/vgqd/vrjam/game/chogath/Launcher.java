package be.vgqd.vrjam.game.chogath;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import be.vgqd.vrjam.game.chogath.Game.SceneType;
import be.vgqd.vrjam.game.chogath.data.modele.Boss;
import be.vgqd.vrjam.game.chogath.data.modele.Bullet;
import be.vgqd.vrjam.game.chogath.data.modele.Champion;
import be.vgqd.vrjam.game.chogath.data.modele.Map;
import be.vgqd.vrjam.game.chogath.data.modele.Power;
import be.vgqd.vrjam.game.chogath.data.modele.Provocations;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.oculus.CameraDisplayData;
import be.vgqd.vrjam.game.chogath.oculus.OculusControllerData;
import be.vgqd.vrjam.game.chogath.oculus.x360.X360ControllerData;

public class Launcher extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		System.out.println(System.getProperty("java.library.path"));
		
		{

			loadData();
		primaryStage.setWidth(CameraDisplayData.SCREENWIDTH);
		primaryStage.setHeight(CameraDisplayData.SCREENHEIGHT);
		primaryStage.setFullScreen(true);
		Game.createInstance(primaryStage);
		primaryStage.setScene(GUI.get(SceneType.HOME));
		primaryStage.show();

		}

		if( !OculusControllerData.isConnected())
		{
			sendErrorDialog("Your Oculus Rift is not connected or already use !"	);
		}
		else if(X360ControllerData.getX360Instance()==null || !OculusControllerData.isConnected())
		{
			sendErrorDialog("Your X360 Controller is undetected");
		}
	}

	private void sendErrorDialog(String msg) {

		Stage dialog = new Stage();
		dialog.setHeight(100);
		dialog.setWidth(300);
		
		dialog.initStyle(StageStyle.TRANSPARENT);
		Group root = new Group();
		Button button = new Button("Exit");
		button.setLayoutX(250);
		button.setLayoutY(60);
		button.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
				
			}
		});
		Text t =  new Text(25, 25,msg);
		t.setFill(Color.GREEN);
		Text t2 = new Text(25, 50,"Please relaunch with correct params.");
		t2.setFill(Color.GREEN);
		root.getChildren().addAll(t,  t2,button);
		Scene scene = new Scene(root);
		scene.setFill(Color.BLACK);
		dialog.setScene(scene);
		dialog.show();
	}

	private void loadData() {

		Map.loadMapData();
		Champion.loadChampionData();
		Provocations.loadProvocations();
		Power.loadPowerData();
		Boss.loadBoss();
		Bullet.loadImages();

	}

	public static void main(String[] args) {
		launch(args);
	}
}
