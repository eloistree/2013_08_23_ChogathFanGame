package be.vgqd.vrjam.game.chogath.utils;

import javafx.geometry.Point2D;

public class Position {
	private final Point2D pInitial;
	private double x, y;

	public Position(double x, double y) {
		pInitial = new Point2D(x, y);

	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public Point2D getInitialPosition()
	{
		return pInitial;
		
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Position [pInitial=");
		builder.append(pInitial);
		builder.append(", x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append("]");
		return builder.toString();
	}
	

}
