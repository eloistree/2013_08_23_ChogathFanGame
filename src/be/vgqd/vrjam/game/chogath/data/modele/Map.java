package be.vgqd.vrjam.game.chogath.data.modele;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Random;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import be.vgqd.vrjam.game.chogath.data.Paths;

public class Map {
	public enum MapType {
		_5V5, _3V3, _5V5Mini
	}

	private String nom;
	private Image imgMap;
	private double width, height;

	private Map(String nom, Image imgMap) {
		if (nom == null || imgMap == null)
			throw new IllegalArgumentException("The argument can't be null.");
		this.nom = nom;
		this.imgMap = imgMap;

	}

	public String getName() {
		return nom;
	}

	public double getHeight() {
		return imgMap.getHeight();
	}

	public double getWidth() {
		return imgMap.getWidth();
	}

	public void setWidth(double d) {

		if (width <= 0)
			width = 0;
		else
			width = d;

	}

	public void setHeight(double d) {
		if (height <= 0)
			height = 0;
		else
			height = d;
	}

	public double getImageHeight() {
		return imgMap.getHeight();
	}

	public double getImageWidth() {
		return imgMap.getWidth();
	}

	public static class NodeBuilder {
		private Map map = null;
		private static double width = 50, height = 50;

		private NodeBuilder() {
		}

		public static NodeBuilder create(Map map) {
			if (map == null)
				throw new NullPointerException();
			NodeBuilder node = new NodeBuilder();
			node.map = map;
			width = (int) map.imgMap.getWidth();
			height = (int) map.imgMap.getHeight();

			return node;

		}

		public NodeBuilder width(double value) {
			map.width = width = value;
			return this;
		}

		public NodeBuilder height(double value) {
			map.height = height = value;
			return this;
		}

		public Node build() {
			ImageView img = new ImageView(map.imgMap);
			img.setFitHeight(height);
			img.setFitWidth(width);
			return img;
		}

	}

	public static Map getMap(MapType type) {
		return maps.get(type);

	}

	public Node getNode() {
		return NodeBuilder.create(this).build();
	}

	public Node getNode(double width, double height) {
		return NodeBuilder.create(this).width(width).height(height).build();
	}

	private static final HashMap<MapType, Map> maps = new HashMap<>();

	public static final void loadMapData() {
		File mapFolder = new File(Paths.IMAGEMAP_PATH);

		if (mapFolder.exists() && mapFolder.isDirectory()) {
			try {

				File f = null;
				Map map = null;
				String name = null;

				name = "Summoner's Rift";
				f = new File(mapFolder.getAbsoluteFile()
						+ "\\5V5_5364x4310.jpg");
				map = new Map(name, new Image(new FileInputStream(f)));
				maps.put(MapType._5V5, map);

				name = "The Twisted Treeline";
				f = new File(mapFolder.getAbsoluteFile()
						+ "\\3V3_2048x1068.jpg");
				map = new Map(name, new Image(new FileInputStream(f)));
				maps.put(MapType._3V3, map);

				name = "Summoner's Rift (mini)";
				f = new File(mapFolder.getAbsoluteFile() + "\\5V5_537x379.png");
				map = new Map(name, new Image(new FileInputStream(f)));

				maps.put(MapType._5V5Mini, map);

			} catch (FileNotFoundException e) {

				e.printStackTrace();
			}

		}

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Map [nom=");
		builder.append(nom);
		builder.append(", imgMap=");
		builder.append(imgMap);
		builder.append(", width=");
		builder.append(width);
		builder.append(", height=");
		builder.append(height);
		builder.append("]");
		return builder.toString();
	}

	public boolean isMapBoundsOk(double x, double y) {

		if (x < 0 || x > imgMap.getWidth())
			return false;
		if (y < 0 || y > imgMap.getHeight())
			return false;

		return true;

	}

	public double getCenterX() {

		return imgMap.getWidth() / 2;
	}

	public double getCenterY() {

		return imgMap.getHeight() / 2;
	}

	static private Random rand = new Random();

	public Point2D getRanomdCornerPosition() {
		boolean isSud = rand.nextBoolean();
		boolean isEast = rand.nextBoolean();
		double x = 0.0, y = 0.0;
		if (isSud)
			y = this.getHeight();
		if (isEast)
			x = this.getWidth();

		return new Point2D(x, y);
	}

	public Point2D getRandomAxePosition() {
		boolean isOnYAxe = rand.nextBoolean();
		double x = 0.0, y = 0.0;
		if (isOnYAxe)
			y = rand.nextInt((int) this.getHeight());
		else
			x = rand.nextInt((int) this.getHeight());

		return new Point2D(x, y);
	}

	public Point2D getRandomCenterEdgePosition() {
		boolean isSud = rand.nextBoolean();
		boolean isEast = rand.nextBoolean();
		double x = 0.0, y = 0.0;

		if (isSud) {
			y = this.getHeight();
			x = this.getCenterX();
		} else if (isEast) {
			y = this.getCenterY();
			x = this.getWidth();
		} else if (!isEast)
			y = this.getCenterY();
		else if (!isSud)
			x = this.getCenterX();

		return new Point2D(x, y);
	}

	public Point2D getCenter() {
		return new Point2D(getCenterX(),getCenterY());
	}

}
