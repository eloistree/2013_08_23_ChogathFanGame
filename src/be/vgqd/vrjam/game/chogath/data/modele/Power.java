package be.vgqd.vrjam.game.chogath.data.modele;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import be.vgqd.vrjam.game.chogath.data.GameData;
import be.vgqd.vrjam.game.chogath.data.GameMovementManager;
import be.vgqd.vrjam.game.chogath.data.InGame;
import be.vgqd.vrjam.game.chogath.data.Paths;
import be.vgqd.vrjam.game.chogath.oculus.OculusControllerData;
import be.vgqd.vrjam.game.chogath.utils.Direction;

public class Power {

	public enum Type {
		POWER1, POWER2, POWER3, POWER4, CAPACITY1, CAPACITY2
	}

	private static final HashMap<Power.Type, Power> powers = new HashMap<>();

	private int duration;
	private int refreshTime;
	private long lastTimeUsed;
	private String nom;
	private Image image;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getRefreshTime() {
		return refreshTime;
	}

	public void setRefreshTime(int refreshTime) {
		this.refreshTime = refreshTime;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	private Power(String nom, Image image, int duration, int refreshtime) {
		if (nom == null || image == null)
			throw new IllegalArgumentException("The argument can't be null.");
		this.nom = nom;
		this.image = image;
		this.duration = duration;
		this.refreshTime = refreshtime;

	}

	protected boolean isUsable(long time) {

		if (lastTimeUsed<=0) {return true;}
		return isRefresh(time);
	}
	public void useIt(long time) {
			lastTimeUsed = time;
	}

	public boolean isFinished(long time) {
		if (lastTimeUsed <= 0)
			return true;
		return time - lastTimeUsed >= duration;
	}

	public boolean isRefresh(long time) {
		if (lastTimeUsed <= 0)
			return true;
		return time - lastTimeUsed >= refreshTime;
	}
	public long getTimeleft(long time) {
		return lastTimeUsed + refreshTime - time;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Power [duration=");
		builder.append(duration);
		builder.append(", refreshTime=");
		builder.append(refreshTime);
		builder.append(", lastTimeUsed=");
		builder.append(lastTimeUsed);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", image=");
		builder.append(image);
		builder.append("]");
		return builder.toString();
	}

	public static class NodeBuilder {
		private Power power = null;
		private static int width = 25, height = 25;

		private NodeBuilder() {
		}

		public static NodeBuilder create(Power power) {
			if (power == null)
				throw new NullPointerException();
			NodeBuilder node = new NodeBuilder();
			node.power = power;

			return node;

		}

		public NodeBuilder width(int value) {
			width = value;
			return this;
		}

		public NodeBuilder height(int value) {
			height = value;
			return this;
		}

		public Node build() {
			// change that by a node.
			ImageView img = new ImageView(power.image);
			img.setFitHeight(height);
			img.setFitWidth(width);
			return img;
		}

		public Power getChamp() {
			return power;
		}

		public void setPower(Power power) {
			this.power = power;
		}

		public static int getWidth() {
			return width;
		}

		public static int getHeight() {
			return height;
		}

	}

	public static Power getPower(Power.Type type) {
		return powers.get(type);

	}

	public Node getNode() {
		return NodeBuilder.create(this).build();
	}

	public static final void loadPowerData() {

		try {

			Image img = new Image(new FileInputStream(Paths.PLAYER_PATH
					+ "\\Shield.png"));
			powers.put(Type.POWER4, new Power("Shield", img, 3000, 12000) {
				@Override
				public void useIt(long time) {
					
					if (isUsable(time)) {
						InGameData.getInstance().getPlayer().setShield(true);
						super.useIt(time);
					}
				}

			});
			img = new Image(new FileInputStream(Paths.PLAYER_PATH
					+ "\\FeralScream.png"));
			powers.put(Type.POWER1, new Power("FeralScream", img, 1000, 12000){
				@Override
				public void useIt(long time) {
					
					if (isUsable(time)) {
						
						Point2D pt = InGame.getInstance().getPlayerPosition();
						Direction d = InGame.getInstance().getPlayerMovingDirection();
						if(Math.abs(d.getX())>0.1 ||Math.abs(d.getY())>0.1 ){
						double x = pt.getX()+(d.getX()*70.0);
						double y = pt.getY()-(d.getY()*70.0);
						double x2 = pt.getX()+(d.getX()*200.0);
						double y2 = pt.getY()-(d.getY()*200.0);
						InGame.getInstance().addBullet(
								Bullet.getBullet(Bullet.Type.FERIALSCREAM)
								,x,y,x2,y2);}
						else{
							
							double x = pt.getX();
							double y = pt.getY();
							Cursor c = Cursor.getOculusCursorInstance();
							double x2 = c.getX();
							double y2 = c.getY();
							InGame.getInstance().addBullet(
									Bullet.getBullet(Bullet.Type.FERIALSCREAM)
									,x,y,x2,y2);
						}
						super.useIt(time);
					}
					
				}

			});

			img = new Image(new FileInputStream(Paths.PLAYER_PATH
					+ "\\Rupture.png"));
			powers.put(Type.POWER2, new Power("Rupture", img, 500, 10000){
				@Override
				public void useIt(long time) {
					
					if (isUsable(time)) {

						Cursor c = Cursor.getOculusCursorInstance();
						InGame.getInstance().addBullet(Bullet.getBullet(Bullet.Type.RUPTURE),c.getX(), c.getY());
						super.useIt(time);
					}
					
				}

			});
			
			img = new Image(new FileInputStream(Paths.PLAYER_PATH
					+ "\\VorpalSpikes.png"));
			powers.put(Type.POWER3, new Power("VorpalSpikes", img, 0, 500));
			img = new Image(new FileInputStream(Paths.PLAYER_PATH
					+ "\\Flash.png"));
			powers.put(Type.CAPACITY1, new Power("Flash", img, 0, 15000) {
				@Override
				public void useIt(long time) {
					
					if (isUsable(time)) {

					Cursor c = Cursor.getOculusCursorInstance();
					InGame.getInstance().teleportPlayer(c.getX(), c.getY());
					OculusControllerData.saveCurrentRYPAsFrontView();
						super.useIt(time);
					}
					
				}

			});
			img = new Image(new FileInputStream(Paths.PLAYER_PATH
					+ "\\Ghost.png"));
			powers.put(Type.CAPACITY2, new Power("Ghost", img, 7000, 15000) {
				@Override
				public void useIt(long time) {
					
					if (isUsable(time)) {

						InGameData.getInstance().getPlayer()
								.setSpeed(GameData.PLAYER_SPEED_WITH_GHOST);
						GameMovementManager.getInstance()
								.setSpeedTo(
										InGameData.getInstance().getPlayer(),
										InGameData.getInstance().getPlayer()
												.getSpeed());
						super.useIt(time);
					}
					
				}

			});

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

	}

	public static Collection<Power> values() {
		return powers.values();
	}
	public static void clean()
	{
		for (Power p : powers.values()) {
			p.reset();
		}
		
		
	}

	private void reset() {

		lastTimeUsed=0;
		
	}
}
