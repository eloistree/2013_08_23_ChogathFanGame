package be.vgqd.vrjam.game.chogath.data;

import java.util.HashMap;
import java.util.Vector;

import be.vgqd.vrjam.game.chogath.data.interfaces.Refreshable;
import be.vgqd.vrjam.game.chogath.game.TimeManager.TimerListener;

public class GameRefreshManager implements TimerListener {

	static final private GameRefreshManager INSTANCE = new GameRefreshManager();

	public static GameRefreshManager getInstance() {
		return INSTANCE;
	}

	private final HashMap<Object, Refreshable> refreshables = new HashMap<>();


	public static void remove(Object o) {
		getInstance().refreshables.remove(o);
	}

	public void store(Object o, Refreshable toRefresh) {
		if (o != null && toRefresh != null)
			refreshables.put(o, toRefresh);
	}

	private Vector<Refreshable> toRefresh	= new Vector<>();
	@Override
	public void ticTac() {
		final long time = GamePopAndTimeManager.getInstance().getTimePlayed();
		toRefresh.clear();
		for (Refreshable r : refreshables.values()) {
			toRefresh.add(r);
		}
		if (toRefresh.size()>0)
		for (Refreshable r : toRefresh) {
			r.refresh(time);
		}

	}

	public static void clean() {getInstance().refreshables.clear();
		
	}

}
