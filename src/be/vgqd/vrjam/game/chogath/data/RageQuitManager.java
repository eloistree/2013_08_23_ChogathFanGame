package be.vgqd.vrjam.game.chogath.data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import be.vgqd.vrjam.game.chogath.data.modele.Champion;

public class RageQuitManager {

	private static RageQuitManager INSTANCE = new RageQuitManager();

	public static RageQuitManager getInstance() {
		return INSTANCE;
	}

	private HashMap<Champion, Integer> death = new HashMap<Champion, Integer>();

	public void championKilled(Champion champ) {
		if (death.get(champ) == null) {
			death.put(champ, new Integer(1));
		} else {
			int v =  death.get(champ).intValue() +1;
			death.put(champ,v);
			if (v>=GameData.RAGEQUITNUMBER)rageQuitInstances.add(champ);
		}
	}

	private final List<Champion> rageQuitInstances = new LinkedList<Champion>();

	public List<Champion> getRageQuitInstanceList() {return rageQuitInstances;
	}

	

	public boolean isConnected(Champion c) {
		if (death.get(c) == null)
			return true;
		return death.get(c).intValue() < GameData.RAGEQUITNUMBER;
	}

	public static void clean() {
		getInstance().death.clear();
		getInstance().rageQuitInstances.clear();
		
	}

	public int getKills(Champion champ) {
		return death.get(champ);
	}

	public int getSizeDeconnected() {
		return rageQuitInstances.size();
	}

}
