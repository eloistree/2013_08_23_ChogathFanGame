package be.vgqd.vrjam.game.chogath.data;

import java.io.File;

public class Paths {
	public static final String APP_PATH = new File("").getAbsolutePath();
	public static final String SOUND_PATH =APP_PATH+"\\data\\sound";
	public static final String IMAGE_PATH =APP_PATH+"\\data\\image";
	public static final String IMAGECHAMPION_PATH =APP_PATH+"\\data\\image\\champion";
	public static final String IMAGEMAP_PATH =APP_PATH+"\\data\\image\\map";
	public static final String PLAYER_PATH = APP_PATH+"\\data\\image\\player";
	public static final String PROVOCATION_PATH = APP_PATH+"\\data\\file";
	
}
