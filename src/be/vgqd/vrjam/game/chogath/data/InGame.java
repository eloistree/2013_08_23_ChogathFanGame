package be.vgqd.vrjam.game.chogath.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Random;

import javafx.animation.Animation.Status;
import javafx.animation.FadeTransition;
import javafx.animation.FadeTransitionBuilder;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.text.Text;
import javafx.util.Duration;
import be.vgqd.vrjam.game.chogath.Game.SceneType;
import be.vgqd.vrjam.game.chogath.data.GameSoundManager.GameSound;
import be.vgqd.vrjam.game.chogath.data.interfaces.Refreshable;
import be.vgqd.vrjam.game.chogath.data.modele.Boss;
import be.vgqd.vrjam.game.chogath.data.modele.Bullet;
import be.vgqd.vrjam.game.chogath.data.modele.Champion;
import be.vgqd.vrjam.game.chogath.data.modele.Cursor;
import be.vgqd.vrjam.game.chogath.data.modele.InGameData;
import be.vgqd.vrjam.game.chogath.data.modele.Map.MapType;
import be.vgqd.vrjam.game.chogath.data.modele.Player;
import be.vgqd.vrjam.game.chogath.data.modele.Power;
import be.vgqd.vrjam.game.chogath.data.modele.Power.Type;
import be.vgqd.vrjam.game.chogath.game.Collider;
import be.vgqd.vrjam.game.chogath.game.Collider.Sensor;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.game.TimeManager;
import be.vgqd.vrjam.game.chogath.game.TimeManager.Timer;
import be.vgqd.vrjam.game.chogath.game.images.Images;
import be.vgqd.vrjam.game.chogath.game.sounds.Sound;
import be.vgqd.vrjam.game.chogath.game.sounds.Sound.SoundType;
import be.vgqd.vrjam.game.chogath.nodes.GameOverNode;
import be.vgqd.vrjam.game.chogath.nodes.KillNode;
import be.vgqd.vrjam.game.chogath.nodes.NextRageQuitListNode;
import be.vgqd.vrjam.game.chogath.nodes.PlayerNode;
import be.vgqd.vrjam.game.chogath.nodes.PowerAndCapacityNode;
import be.vgqd.vrjam.game.chogath.nodes.ProvocationNode;
import be.vgqd.vrjam.game.chogath.nodes.RageQuitListNode;
import be.vgqd.vrjam.game.chogath.nodes.RageQuitNode;
import be.vgqd.vrjam.game.chogath.nodes.Texts;
import be.vgqd.vrjam.game.chogath.nodes.TimeNode;
import be.vgqd.vrjam.game.chogath.oculus.CameraDisplayData;
import be.vgqd.vrjam.game.chogath.oculus.OculusScene;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;
import be.vgqd.vrjam.game.chogath.oculus.StereoNodeBuilder;
import be.vgqd.vrjam.game.chogath.utils.Direction;
import be.vgqd.vrjam.game.chogath.utils.Position;

public class InGame implements Refreshable {

	private final static InGame INSTANCE = new InGame();

	public static final InGame getInstance() {
		return INSTANCE;
	}

	private final InGameData _ingameData = InGameData.getInstance();
	private final GameColliderManager _coliMan = GameColliderManager
			.getInstance();
	private final RageQuitManager _rageMan = RageQuitManager.getInstance();
	private final GameMovementManager _moveMan = GameMovementManager
			.getInstance();
	private final GamePopAndTimeManager _popMan = GamePopAndTimeManager
			.getInstance();
	private final GameRefreshManager _refreshMan = GameRefreshManager
			.getInstance();
	private final GameBossMoveManager _bossMovementMan = GameBossMoveManager
			.getInstance();

	private Group gl, gr;
	private StereoNode<Node> groupMap;
	private StereoNode<Node> cursor;
	private StereoNode<TimeNode> time;
	private StereoNode<RageQuitNode> rage;
	private StereoNode<KillNode> kill;

	private StereoNode<GameOverNode> gameover;
	private StereoNode<ProvocationNode> provocation;
	private StereoNode<Text> level;
	private StereoNode<RageQuitListNode> rageQuit;
	private StereoNode<NextRageQuitListNode> nextRageQuit;
	private StereoNode<PowerAndCapacityNode> powers;
	private StereoNode<ImageView> imageX360;

	private Timer refreshTimer, quickRefreshTimer;

	private InGame() {

		_ingameData.setMap(MapType._5V5);
		OculusScene os = (OculusScene) GUI.get(SceneType.INGAME);
		addBackground();
		// Ajouter curosr
		Cursor c = Cursor.getOculusCursorInstance();
		cursor = os.add(c.getNode(), c.getNode(), 0, 0);

		// Ajouter la carte
		gl = new Group();
		gr = new Group();

		groupMap = os.add(gl, gr, 0, 0);
		linkManagedToTime();

	}

	private void linkManagedToTime() {

		refreshTimer = new Timer(GameData.FPS_REFRESH_RECORDER);
		refreshTimer.addListener(_coliMan);
		refreshTimer.addListener(_popMan);
		refreshTimer.addListener(_refreshMan);
		refreshTimer.addListener(_bossMovementMan);

		quickRefreshTimer = new Timer(GameData.FPS_REFRESH_IMPORTAN_OBJECT);
		quickRefreshTimer.addListener(_moveMan);

	}

	public void resetGame() {

		if (refreshTimer != null)
			refreshTimer.stop();
		if (quickRefreshTimer != null)
			quickRefreshTimer.stop();
		cleanScreen();
		TimeManager.clean();
		GUI.clean();
		Collider.clean();
		Boss.clean();
		GameRefreshManager.clean();
		GameMovementManager.clean();
		GamePopAndTimeManager.clean();
		GameData.clean();
		RageQuitManager.clean();
		GameBossMoveManager.clean();
		Power.clean();

		InGameData.clean();

		_ingameData.setGameStarted(true);

		// Add group stereo to the GUI
		GUI.storeAndWriteOver(InGameData.getInstance(), groupMap);

		StereoNode<Node> map = GUI.get(_ingameData);
		gl = (Group) map.getLeft();
		gr = (Group) map.getRight();
		// Display the map the map
		gl.getChildren().add(_ingameData.getMap().getNode());
		gr.getChildren().add(_ingameData.getMap().getNode());
		// Add curosr to gui
		Cursor c = Cursor.getOculusCursorInstance();
		GUI.store(c, cursor);
		cursor.getLeft().toFront();
		cursor.getRight().toFront();

		addPlayer();
		addTheInterface();

	}

	public void start() {

		resetGame();
		_popMan.startRecoardingData();
		_coliMan.startDetection();
		_moveMan.startMovingObject();
		_refreshMan.store(this, this);
		refreshTimer.start();
		quickRefreshTimer.start();

	}

	private void setGameOver() {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {

			refreshGameOver(_popMan.getTimePlayed(), _ingameData.getRagequit(),
					_ingameData.getKill());
			Sound.stop();
			Sound.play("sound_gameover.wav", SoundType.SOUND, 2);
			stop();
			_ingameData.setGameover(true);
			_ingameData.setGameStarted(false);
		}
	}

	public void exitTheGame() {
		setGameOver();
	}

	public void stop() {

		_ingameData.setGameStarted(false);
		for (Timer t : TimeManager.getTimers()) {
			t.stop();
		}
		_moveMan.stopMovingObject();
		_coliMan.stopDetection();
		_popMan.stopRecordingData();
	}

	public void movePlayer(Direction direction) {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {
			Direction d = _moveMan.getDirectionOf(_ingameData.getPlayer());
			if (d != null)
				d.joinData(direction);

		}
	}

	private Position ptTmp = new Position(0, 0);
	private Direction dTmp = new Direction();

	public void movePlayer(double x, double y) {
		movePlayer(x, y, 1.8);
	}

	public void movePlayer(double x, double y, double intensity) {
		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {

			reinitPointToThisObjectLocation(_ingameData.getPlayer(), ptTmp);
			Direction.setDirection(dTmp, ptTmp.getX(), ptTmp.getY(), x, y);
			dTmp.setIntensity(intensity);
			dTmp.inverseY();
			movePlayer(dTmp);

		}
	}

	public void reinitPointToThisObjectLocation(Object o, Position pt) {
		StereoNode<Node> sn = GUI.get(o);
		pt.setX(sn.getLayoutX() + Collider.get(sn).getX());
		pt.setY(sn.getLayoutY() + Collider.get(sn).getY());

	}

	public void teleportPlayer(double x, double y) {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {
			StereoNode<Node> snPlayer = GUI.get(_ingameData.getPlayer());

			if (x < 0)
				x = 0;
			if (y < 0)
				y = 0;
			if (x > _ingameData.getMap().getImageWidth())
				x = _ingameData.getMap().getImageWidth();
			if (y > _ingameData.getMap().getImageHeight())
				y = _ingameData.getMap().getImageHeight();
			snPlayer.setLayout(x, y);

		}
	}

	public void moveCamera(Direction direction) {

		StereoNode<Node> map = GUI.get(_ingameData);

		double x = map.getLayoutX();
		double y = map.getLayoutY();
		map.setLayout(x + direction.getX() * 3, y - direction.getY() * 3);
	};

	public void addBoss(Champion ch) {
		if (ch != null)
			addBoss(Boss.getBoss(ch));
	}

	public void addBoss(Boss boss) {
		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {

			if (boss != null /* && _rageMan.isConnected(boss.getChampion()) */) {

				Node bossl = boss.getNode(), bossr = boss.getNode();
				Point2D pt = getRandomPositionOnMap(50, 70, 50, 50);

				StereoNode<Node> snBoss = StereoNodeBuilder.getStereoNode(
						bossl, bossr, pt.getX(), pt.getY());
				GUI.storeAndWriteOver(boss, snBoss);

				double width = bossl.getBoundsInLocal().getWidth() / 2;
				Collider.store(snBoss, width / 2
						* GameData.PCT_HITBOX_RADIUS_BOSS);
				_bossMovementMan.store(boss);
				_moveMan.addMovingOject(boss,
						_bossMovementMan.getDirectionOf(boss),
						GameData.DEFAUTSPEED);

				addToTheMap(snBoss);
				boss.setPopped(true);
				GameSoundManager.play(GameSound.BOSS_APPARITION);

				_refreshMan.store(boss, boss);

			}

		}
	}

	public void addChampion() {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {
			Champion ch = getValideChamp();
			if (ch != null) {
				Node chl = ch.getNode(), chr = ch.getNode();
				Point2D pt = getRandomPositionOnMap(50, 70, 50, 50);

				StereoNode<Node> snChamp = StereoNodeBuilder.getStereoNode(chl,
						chr, pt.getX(), pt.getY());
				GUI.storeAndWriteOver(ch, snChamp);

				Collider.store(snChamp, 0, 0, Champion.NodeBuilder.getWidth()
						/ 2 * GameData.PCT_HITBOX_RADIUS_PLAYER);

				addToTheMap(GUI.get(ch));
				GameSoundManager.play(GameSound.CHAMPION_APPARITION);
			}

		}
	}

	private Champion getValideChamp() {
		Champion ch = null;

		int i = 0;
		while (ch == null) {
			ch = Champion.getChampion();
			if (GUI.get(ch) != null || !_rageMan.isConnected(ch))
				ch = null;
			if (ch != null && GUI.get(Boss.getBoss(ch)) != null) {
				ch = null;
			}
			if (ch != null)
				return ch;
			if (i++ >= 10)
				break;
		}
		if (ch == null) {
			for (Champion champ : Champion.getChampionsInstanceList()) {
				if (GUI.get(ch) != null && _rageMan.isConnected(ch))
					return champ;

			}
		}

		return null;
	}

	public void addPlayer() {

		Player player = _ingameData.getPlayer();

		// Ajouter player

		StereoNode<Node> playerSn = StereoNodeBuilder.getStereoNode(
				(Node) PlayerNode.getNode(), (Node) PlayerNode.getNode(), 100,
				100);
		GUI.storeAndWriteOver(player, playerSn);
		Collider.store(playerSn, PlayerNode.RADIUS);
		GameMovementManager.store(player, new Direction(0.0, 0.0),
				GameData.PLAYER_SPEED);
		addToTheMap(playerSn);
	}

	private Point2D getRandomPositionOnMap(int paddingTop, int paddingRight,
			int paddingBot, int paddingLeft) {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {
			final int width = (int) _ingameData.getMap().getImageWidth(), height = (int) _ingameData
					.getMap().getImageHeight();
			int x = 0, y = 0;
			Random r = new Random();
			x = paddingLeft + r.nextInt(width - paddingRight - 50);
			y = paddingTop + r.nextInt(height - paddingBot - 50);

			return new Point2D(x, y);
		}
		return null;
	}

	// must be private
	public void addToTheMap(StereoNode<? extends Node> sn) {

		if (sn != null)
			if (_ingameData != null && _ingameData.isGameStarted()
					&& !_ingameData.isPause()) {
				gl.getChildren().add(sn.getLeft());
				gr.getChildren().add(sn.getRight());
			}
	}

	public void playerKillChampion(Champion champ) {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {

			_ingameData.getPlayer().miamMiam();

			checkPlayerState();
			GameSoundManager.play(GameSound.CHOGATH_MIAM);
			_rageMan.championKilled(champ);
			_ingameData.plusOneKill();
			refreshKillNode(_ingameData.getKill());
			int i = _rageMan.getKills(champ);
			i = i > GameData.RAGEQUITNUMBER ? -2 : i;
			refreshNextRageQuitListNode(champ, i);
			refreshRageQuitListNode(champ, i);
			if (!_rageMan.isConnected(champ)) {
				_ingameData.plusRageQuit();
				refreshRageQuitNode(champ.getImage(), _ingameData.getRagequit());
			}
			boolean rage = !_rageMan.isConnected(champ);
			if (rage && InGameData.getRandomInstance().nextInt(5) == 1) {
				refreshProvocation(champ, "Fuck this game, I quit");
			}

			askToRemove(champ);
		}
	}

	public void addMovingAttack() {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {

			// in the future near, this fonction will choose between lux,
			// ezreal,
			// nidalee ... attack with 0.x chance
			addBullet(Bullet.Type.JAVELIN);
		}
	}

	public void playerHitBoss(Boss b) {
		b.hit();
		if (b.isAlive()) {
			StereoNode<Node> sn = GUI.get(b);
			Point2D pt = getRandomPositionOnMap(0, 0, 0, 0);
			if (sn != null)
				sn.setLayout(pt.getX(), pt.getY());
		} else {
			askToRemove(b);
		}

	}

	public void bossHitPlayer(Boss b) {
		Player p = _ingameData.getPlayer();

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause() && !p.isShieldOn()) {

			b.meleeAttackBehavoir();

		}
	}

	public void checkPlayerState() {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {
			long time = _popMan.getTimePlayed();
			Player p = _ingameData.getPlayer();
			StereoNode<Node> sn = GUI.get(p);
			PlayerNode left = (PlayerNode) sn.getLeft();
			PlayerNode right = (PlayerNode) sn.getRight();
			Power po = null;
			if (p.getSpeed() != GameData.PLAYER_SPEED) {
				po = Power.getPower(Type.CAPACITY2);

				if (po != null) {

					if (po.isFinished(time)) {
						_ingameData.getPlayer().setSpeed(GameData.PLAYER_SPEED);

						_moveMan.setSpeedTo(_ingameData.getPlayer(),
								_ingameData.getPlayer().getSpeed());
					}

				}
			}
			if (p.isShieldOn()) {
				po = Power.getPower(Type.POWER4);

				if (po != null) {

					if (po.isFinished(time)) {
						_ingameData.getPlayer().setShield(false);

					}
				}
			}
			if (p.getLife() <= 0)
				this.setGameOver();
			left.setShield(p.isShieldOn());
			right.setShield(p.isShieldOn());
			left.setLife(p.getLife());
			right.setLife(p.getLife());

		}
	}

	public void askToRemove(Object o) {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {
			if (o instanceof Bullet) {
				((Bullet) o).disparitionBehaviour();
			}
			Collider.remove(o);
			GameMovementManager.remove(o);
			GameRefreshManager.remove(o);
			GameBossMoveManager.remove(o);
			StereoNode<Node> sn = GUI.remove(o);
			gl.getChildren().remove(sn.getLeft());
			gr.getChildren().remove(sn.getRight());
		}
	}

	private void cleanScreen() {

		for (int i = gl.getChildren().size() - 1; i >= 0; i--) {
			gl.getChildren().remove(i);
		}

		for (int i = gr.getChildren().size() - 1; i >= 0; i--) {
			gr.getChildren().remove(i);
		}
		OculusScene os = (OculusScene) GUI.get(SceneType.INGAME);
		if (gameover != null)
			os.remove(gameover);
		if (rageQuit != null)
			os.remove(rageQuit);
		if (provocation != null)
			os.remove(provocation);
		if (imageX360 != null)
			os.remove(imageX360);

	}

	private void addBackground() {
		OculusScene os = (OculusScene) GUI.get(SceneType.INGAME);
		Image im = null;
		try {
			im = new Image(new FileInputStream(new File(Paths.IMAGE_PATH
					+ "\\ingamebackground.jpg")));

			ImageViewBuilder<?> img = ImageViewBuilder.create().image(im)
					.layoutX(-CameraDisplayData.SCREENWIDTH * 1 / 3)
					.layoutY(-CameraDisplayData.SCREENHEIGHT / 2)
					.fitWidth(CameraDisplayData.SCREENWIDTH)
					.fitHeight(CameraDisplayData.SCREENHEIGHT);
			os.add(img.build(), img.build());

		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void addOneLevelOfDifficulty() {
		_ingameData.addOneLevelOfDifficulty();
		int level = _ingameData.getLevel();
		refreshLevel(level);
		switch (level) {
		case 0:
			Sound.play("sound_level1.mp3", SoundType.SOUND);

			break;

		case 4:
			Sound.play("sound_level4.mp3", SoundType.SOUND);

			break;

		case 9:
			Sound.play("sound_level8.mp3", SoundType.SOUND);

			break;

		case 14:
			Sound.play("sound_level15.mp3", SoundType.SOUND);

			break;
		case 19:
			// Sound.play("sound_domination.mp3", SoundType.SOUND);

			break;

		}

	}

	private void addTheInterface() {

		OculusScene os = (OculusScene) GUI.get(SceneType.INGAME);

		double x = 0;
		double y = -20;
		TimeNode tl = TimeNode.getNode();
		TimeNode tr = TimeNode.getNode();
		time = StereoNodeBuilder.getStereoNode(tl, tr, x, y);

		x = 200;
		y = -20;
		RageQuitNode rl = RageQuitNode.getNode(true);
		RageQuitNode rr = RageQuitNode.getNode(true);
		rage = StereoNodeBuilder.getStereoNode(rl, rr, x, y);

		x = 100;
		y = -20;
		KillNode kl = KillNode.getNode();
		KillNode kr = KillNode.getNode();
		kill = StereoNodeBuilder.getStereoNode(kl, kr, x, y);

		ProvocationNode pl = ProvocationNode.getNode();
		ProvocationNode pr = ProvocationNode.getNode();
		x = -pl.getBoundsInLocal().getWidth() / 2;
		y = +90;

		provocation = StereoNodeBuilder.getOculusStereoNode(pl, pr, x, y, 0);

		Image img = Images.getImage("\\x360\\X360.png");
		double width = CameraDisplayData.getWidth() / 2;
		double height = img.getHeight() * width / img.getWidth();
		ImageViewBuilder<?> imgBuilder = ImageViewBuilder.create().opacity(1.0)
				.image(img).fitHeight(height).fitWidth(width);
		ImageView x360imgL = imgBuilder.build();
		ImageView x360imgR = imgBuilder.build();
		x = -x360imgL.getFitWidth() / 2;
		y = +50;

		imageX360 = StereoNodeBuilder.getOculusStereoNode(x360imgL, x360imgR,
				x, y, 0);

		Text levell = Texts.getTitlebuilder().text("level 0").build();
		Text levelr = Texts.getTitlebuilder().text("level 0").build();
		x = _ingameData.getMap().getWidth()
				- levell.getBoundsInLocal().getWidth();
		y = -20;
		level = StereoNodeBuilder.getStereoNode(levell, levelr, x, y);
		if (gameover != null)
			gameover.setVisible(false);
		GameOverNode gol = GameOverNode.getNode();
		GameOverNode gor = GameOverNode.getNode();

		x = -gol.getBoundsInLocal().getWidth() / 2;
		y = -gol.getBoundsInLocal().getHeight() / 2;
		gameover = StereoNodeBuilder.getOculusStereoNode(gol, gor, x, y, 0);

		provocation.getLeft().setOpacity(0.0);
		provocation.getRight().setOpacity(0.0);
		gameover.getLeft().setOpacity(0.0);
		gameover.getRight().setOpacity(0.0);
		gameover.setVisible(false);
		RageQuitListNode imgVL = RageQuitListNode.getNode();
		RageQuitListNode imgVR = RageQuitListNode.getNode();

		x = -imgVL.getBoundsInLocal().getWidth() / 2;
		y = -imgVR.getBoundsInLocal().getHeight() / 2;
		rageQuit = StereoNodeBuilder.getOculusStereoNode(imgVL, imgVR, x, y, 0);
		rageQuit.setOpacity(0.0);
		NextRageQuitListNode nextRageVL = NextRageQuitListNode.getNode();
		NextRageQuitListNode nextRageVR = NextRageQuitListNode.getNode();
		x = -nextRageVL.getBoundsInLocal().getWidth() - 5;
		y = 0;
		nextRageQuit = StereoNodeBuilder.getStereoNode(nextRageVL, nextRageVR,
				x, y);

		PowerAndCapacityNode powersVL = PowerAndCapacityNode.getNode();
		PowerAndCapacityNode powersVR = PowerAndCapacityNode.getNode();
		x = _ingameData.getMap().getWidth() + 5;
		y = +10;
		powers = StereoNodeBuilder.getStereoNode(powersVL, powersVR, x, y);

		addToTheMap(time);
		addToTheMap(powers);
		addToTheMap(rage);
		addToTheMap(kill);
		addToTheMap(level);
		addToTheMap(nextRageQuit);

		os.add(provocation);
		pl.toFront();
		pr.toFront();
		os.add(gameover);
		os.add(rageQuit);
		imgVL.toFront();
		imgVR.toFront();
		os.add(imageX360);

	}

	public void refreshPowers(Power.Type type, double complet) {
		powers.getLeft().set(type, complet);
		powers.getRight().set(type, complet);

	}

	public void refreshRageQuitNode(Image img, int value) {
		rage.getLeft().setText(img, value);
		rage.getRight().setText(img, value);

	}

	public void refreshRageQuitListNode(Champion champ, int value) {
		rageQuit.getLeft().setText(champ, value);
		rageQuit.getRight().setText(champ, value);

	}

	public void refreshNextRageQuitListNode(Champion champ, int value) {
		nextRageQuit.getLeft().setText(champ, value);
		nextRageQuit.getRight().setText(champ, value);

	}

	public void refreshKillNode(int value) {
		kill.getLeft().setText(value);
		kill.getRight().setText(value);

	}

	public void refreshTimeNode(long value) {

		time.getLeft().setText(value);
		time.getRight().setText(value);
	}

	public void refreshLevel(int value) {
		String s = "Level " + value;
		level.getLeft().setText(s);
		level.getRight().setText(s);

	}

	private void refreshGameOver(long timePlayed, int ragequit, int kill) {
		gameover.getLeft().setText(timePlayed, ragequit, kill);
		gameover.getLeft().setOpacity(1.0);
		gameover.getLeft().setVisible(true);
		gameover.getRight().setText(timePlayed, ragequit, kill);
		gameover.getRight().setOpacity(1.0);
		gameover.getRight().setVisible(true);

		gameover.getLeft().toFront();
		gameover.getRight().toFront();
	}

	// START///TEST TO REMOVE
	private FadeTransition fadeLeft = FadeTransitionBuilder.create()
			.duration(Duration.seconds(5)).delay(Duration.seconds(2))
			.fromValue(8.0).toValue(0.0)
			.onFinished(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {

					fadeLeft.stop();
				}
			}).build();

	private FadeTransition fadeRight = FadeTransitionBuilder.create()
			.duration(Duration.seconds(5)).delay(Duration.seconds(2))
			.fromValue(8.0).toValue(0.0)
			.onFinished(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					fadeRight.stop();
				}
			}).build();

	// //END///Test To remove
	private void refreshProvocation(String msg) {
		provocation.getLeft().setOpacity(1);
		provocation.getLeft().setText(msg);
		provocation.getRight().setOpacity(1);
		provocation.getRight().setText(msg);
		fadeLeft.setNode(provocation.getLeft());
		fadeRight.setNode(provocation.getRight());

		fadeLeft.playFromStart();
		fadeRight.playFromStart();
	}

	public void refreshProvocation(Champion champ, String msg) {

		provocation.getLeft().setChampion(champ);
		provocation.getRight().setChampion(champ);
		refreshProvocation(msg);

	}

	public void refreshProvocation(Boss boss, String msg) {

		provocation.getLeft().setBoss(boss);
		provocation.getRight().setBoss(boss);
		refreshProvocation(msg);
	}

	public void displayInterface(double value) {
		boolean provocationModifAllow = fadeLeft.getStatus() != Status.RUNNING;
		if (value > 0.01 && value < 0.08) {
			rageQuit.setOpacity(0.0);
			if (provocationModifAllow)
				provocation.setOpacity(0.0);

			imageX360.setOpacity(0);
		}
		if (value < 0.4) {
			if (provocationModifAllow)
				provocation.setOpacity(value * 4);
			rageQuit.setOpacity(0.0);

			imageX360.setOpacity(0);
		} else if (value < 0.8) {
			if (provocationModifAllow)
				provocation.setOpacity(0.0);
			rageQuit.setOpacity((value - 0.4) * 4);

			imageX360.setOpacity(0);

		} else if (value >= 0.8) {
			if (provocationModifAllow)
				provocation.setOpacity(0.0);
			rageQuit.setOpacity(0.0);
			imageX360.setOpacity((value - 0.8) * 8);
			if (value >= 0.94) {

				fadeLeft.stop();
				fadeRight.stop();

			}
		}

	}

	public Point2D getPlayerPosition() {

		StereoNode<Node> sn = GUI.get(_ingameData.getPlayer());
		return new Point2D(sn.getLayoutX(), sn.getLayoutY());
	}

	public void pause() {
		if (_ingameData.isGameover()) {
			start();
		} else {
			_ingameData.setPause();

			// TODO Remplacer par un autre system qui freeze dans les managers
			if (_ingameData.isPause()) {
				refreshTimer.stop();
				quickRefreshTimer.stop();
			} else {
				refreshTimer.start();
				quickRefreshTimer.start();
			}
		}

	}

	public Bullet addBullet(Bullet.Type type) {
		Bullet b = Bullet.getBullet(type);

		return addBullet(b, true);

	}

	public <E extends Object> Bullet addBullet(Bullet b, double x, double y) {
		return addBullet(b, x, y, x, y);

	}

	public <E extends Object> Bullet addBullet(Bullet b, E from, E to) {
		if (b != null && from != null && to != null) {
			double ox = 0.0, oy = 0.0;
			double dx = 0.0, dy = 0.0;

			StereoNode<Node> snTmp = GUI.get(from);
			ox = snTmp.getLayoutX();
			oy = snTmp.getLayoutY();
			snTmp = GUI.get(to);
			dx = snTmp.getLayoutX();
			dy = snTmp.getLayoutY();
			return addBullet(b, ox, oy, dx, dy);
		}
		return null;

	}

	public Bullet addBullet(Bullet b, double ox, double oy, double dx, double dy) {

		{
			Direction d = b.getDirection();
			Direction.setDirection(d, ox, -oy, dx, -dy);

			Node chl = b.getNode(), chr = b.getNode();
			StereoNode<Node> sn = StereoNodeBuilder.getStereoNode(chl, chr, ox,
					oy);

			GUI.storeAndWriteOver(b, sn);

			Collider.store(sn, 0, 0, b.getCollisionRadius());

			GameMovementManager.store(b, b.getDirection(), b.getSpeed());

			b.apparitionBehaviour();
			addToTheMap(GUI.get(b));
			return b;
		}
	}

	public Bullet addBullet(Bullet b, boolean random) {

		if (_ingameData != null && _ingameData.isGameStarted()
				&& !_ingameData.isPause()) {
			Point2D pt1, pt2;

			// a random point in the map not on the player
			pt1 = null;
			if (b.getStartPositionWanted() != null) {
				pt1 = b.getStartPositionWanted();
			} else {
				while (pt1 == null) {
					pt1 = getRandomPositionOnMap(0, 0, 0, 0);
					if (isCollidingWithPlayer(pt1.getX(), pt1.getY(),
							b.getApparitionMinDistance()))
						pt1 = null;
				}
			}
			// if random, take a other random point
			if (b.getDirectionWanted() != null) {
				pt2 = Direction.getDestination(pt1, b.getDirectionWanted(), 20);

			} else if (random) {
				pt2 = getRandomPositionOnMap(0, 0, 0, 0);
			} else {
				pt2 = getPlayerPosition();
			}

			return addBullet(b, pt1.getX(), pt1.getY(), pt2.getX(), pt2.getY());
		}
		return null;
	}

	private Sensor lastPlayersensor = null;
	private StereoNode<Node> lastPlayerSN = null;

	public boolean isCollidingWithPlayer(double ox, double oy, double radius) {

		if (lastPlayersensor == null || lastPlayerSN == null) {
			lastPlayersensor = Collider.get(GUI.get(_ingameData.getPlayer()));
			lastPlayerSN = GUI.get(_ingameData.getPlayer());
		}

		double x = lastPlayerSN.getLayoutX() + lastPlayersensor.getX();
		double y = lastPlayerSN.getLayoutY() + lastPlayersensor.getY();

		return Collider.areThereCollision(x, y, lastPlayersensor.getRadius(),
				ox, oy, radius);

	}

	private Direction lastPlayerDirection = new Direction();

	public Direction getPlayerDirection(double ox, double oy) {
		Sensor playersensor = Collider.get(GUI.get(_ingameData.getPlayer()));
		StereoNode<Node> player = GUI.get(_ingameData.getPlayer());

		double x = player.getLayoutX() + playersensor.getX() - ox;
		double y = player.getLayoutY() + playersensor.getY() - oy;

		lastPlayerDirection.setDirectionOnAngle(360 - Direction.getAngle(x, y));
		lastPlayerDirection.setIntensity(1.0 + _ingameData.getDifficulty());
		return lastPlayerDirection;

	}

	public void attack(double x, double y) {

	}

	public void activatePower(Type type) {
		long time = _popMan.getTimePlayed();
		Power p = Power.getPower(type);
		if (time > 0 && p != null) {

			p.useIt(time);

		}

	}

	@Override
	public void refresh(long time) {
		for (Power.Type t : Power.Type.values()) {
			Power p = Power.getPower(t);
			long timeleft = p.getTimeleft(time);
			refreshPowers(t,
					1.0 - (double) timeleft / (double) p.getRefreshTime());
		}
		checkPlayerState();

	}

	public void sendProvocation(Boss boss, String msg) {

		refreshProvocation(boss, msg);

	}

	public void sendProvocation(Champion champ, String msg) {

		refreshProvocation(champ, msg);

	}

	public boolean isInPause() {
		// TODO Auto-generated method stub
		return _ingameData == null || !_ingameData.isGameStarted()
				|| _ingameData.isPause();
	}

	public Point2D getValideRandomPoint(double apparitionMinDistance) {
		Point2D pt1 = null;
		while (pt1 == null) {
			pt1 = getRandomPositionOnMap(0, 0, 0, 0);
			if (isCollidingWithPlayer(pt1.getX(), pt1.getY(),
					apparitionMinDistance))
				pt1 = null;
		}
		return pt1;
	}

	public boolean bulletCollision(Bullet b) {

		StereoNode<Node> sn = GUI.get(b);
		if (sn != null) {
			List<StereoNode<Node>> l = Collider.areThereCollision(sn);
			synchronized (l) {

				if (l != null) {
					for (StereoNode<Node> ste : l) {
						Object o = GUI.found(ste);
						if (o != null) {
							if (b.isHitObject() && o instanceof Bullet) {
								if (((Bullet) o).getType() != Bullet.Type.BUSH)
									;
								askToRemove(o);
							} else if (b.isHitChampion()
									&& o instanceof Champion) {
								askToRemove(o);
							} else if (b.isHitBoss() && o instanceof Boss) {

								playerHitBoss((Boss) o);
							} else if (b.isHitPlayer() && o instanceof Player) {

								b.collisionBehaviour(o);
							}

						}

					}
					if (l.size() > 0)
						return true;

				}
			}
		}
		return false;

	}

	public Direction getPlayerMovingDirection() {
		return _moveMan.getDirectionOf(_ingameData.getPlayer());
	}

}
