package be.vgqd.vrjam.game.chogath.data;

import java.util.List;

import javafx.scene.Node;
import be.vgqd.vrjam.game.chogath.data.modele.Boss;
import be.vgqd.vrjam.game.chogath.data.modele.Bullet;
import be.vgqd.vrjam.game.chogath.data.modele.Champion;
import be.vgqd.vrjam.game.chogath.data.modele.InGameData;
import be.vgqd.vrjam.game.chogath.data.modele.Player;
import be.vgqd.vrjam.game.chogath.game.Collider;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.game.TimeManager.TimerListener;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;

public class GameColliderManager implements TimerListener {

	private static GameColliderManager INSTANCE = new GameColliderManager();

	public static GameColliderManager getInstance() {
		return INSTANCE;
	}

	private boolean isCheckingCollision;

	@Override
	public void ticTac() {

		if (isCheckingCollision)
			checkPlayerCollisions();

	}

	private void checkPlayerCollisions() {
		Player p = InGameData.getInstance().getPlayer();
		List<StereoNode<Node>> l = Collider.areThereCollision(GUI.get(p));
		if (l != null) {
			for (StereoNode<Node> ste : l) {
				Object o = GUI.found(ste);
				if(o!=null)
				{
				if ( o instanceof Bullet) {
					
					((Bullet) o).collisionBehaviour(p);
				}
				else if( o instanceof Champion)
				{
					InGame.getInstance().playerKillChampion((Champion)o);
				}
				else if( o instanceof Boss )
				{
					Boss b = (Boss)o;
					if (b.isMelee())
					{
						InGame.getInstance().bossHitPlayer(b);
						
					}
					else 
					InGame.getInstance().playerHitBoss(b);
				}
				
				}

			}

		}
	}

	public void startDetection() {
		isCheckingCollision = true;

	}

	public void stopDetection() {
		isCheckingCollision = false;

	}

}
