package be.vgqd.vrjam.game.chogath.data.interfaces;

public interface Refreshable {

	public void refresh(long time);
}
