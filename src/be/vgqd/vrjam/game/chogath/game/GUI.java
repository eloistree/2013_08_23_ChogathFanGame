package be.vgqd.vrjam.game.chogath.game;

import java.util.HashMap;

import javafx.scene.Node;
import javafx.scene.Scene;
import be.vgqd.vrjam.game.chogath.Game.SceneType;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;

public class GUI {

	
	private GUI(){throw new IllegalStateException("This constructor should never be call and protect the class to be instancied");}
	public static HashMap< Object , StereoNode<Node>> stereonodes = new HashMap<Object, StereoNode<Node>>();
	public static void store ( Object o, StereoNode<Node> composants	)
	{
		if (stereonodes.get(o)==null)
		stereonodes.put(o, composants);
	}
	public static void storeAndWriteOver ( Object o, StereoNode<Node> composants	)
	{
		if (stereonodes.get(o)!=null)
			stereonodes.remove(o);
		stereonodes.put(o, composants);
		
	}
	public static <E extends Object> StereoNode<Node> get ( E o	)
	{

		return stereonodes.get(o);
	}
	
	
	
	public static HashMap< SceneType , Scene> scenes = new HashMap< SceneType , Scene>();
	public static void store(SceneType type, Scene scene){scenes.put(type, scene);}
	public static Scene get ( SceneType type	)
	{
		return scenes.get(type);
	}
	public static void clean() {
		
		stereonodes.clear();
		
	}
	public static Object found(StereoNode<Node> value) {
		
		if(stereonodes.containsValue(value))
		{
			StereoNode<Node> sn =null;
			for (Object o : stereonodes.keySet()) {
				sn= stereonodes.get(o);
				if (sn!=null && sn==value)return o;
			}

		}
		return null;
	}
	public static StereoNode<Node> remove(Object o) {
		
		return stereonodes.remove(o);
		
	}
	

	

}
