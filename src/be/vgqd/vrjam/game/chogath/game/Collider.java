package be.vgqd.vrjam.game.chogath.game;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javafx.scene.Node;
import be.vgqd.vrjam.game.chogath.oculus.StereoNode;

public class Collider {

	private Collider() {
		throw new IllegalStateException(
				"This constructor should never be call and protect the class to be instancied");
	}

	public static HashMap<StereoNode<Node>, Sensor> colliders = new HashMap<StereoNode<Node>, Sensor>();

	public static void store(StereoNode<Node> o) {
		store(o, 0, 0, -1);
	}

	public static void store(StereoNode<Node> o, double radius) {
		store(o, 0, 0, radius);

	}

	public static void store(StereoNode<Node> o, double x, double y,
			double radius) {
		if (o == null)
			throw new NullPointerException();
		Sensor s = new Sensor(x, y, radius > 0 ? radius : Sensor.DEFAULT_RADIUS);
		store(o, s);

	}

	public static void store(StereoNode<Node> o, Sensor collisionSensor) {

		if (o == null || collisionSensor == null)
			throw new NullPointerException();
		if (colliders.get(o) == null)
			colliders.put(o, collisionSensor);
	}

	public static void storeAndWriteOver(StereoNode<Node> o,
			Sensor collisionSensor) {
		if (o == null || collisionSensor == null)
			throw new NullPointerException();

		if (colliders.get(o) != null)
			colliders.remove(o);
		colliders.put(o, collisionSensor);

	}

	public static <E extends StereoNode<Node>> Sensor get(E o) {

		return colliders.get(o);
	}

	public static void clean() {

		colliders.clear();

	}

	public static class Sensor {
		public Sensor(double x, double y, double radius) {
			this.x = x;
			this.y = y;
			this.radius = radius;
		}

		public static final double DEFAULT_RADIUS = 10;
		private double radius;
		private double x;
		private double y;

		public double getRadius() {
			return radius;
		}

		public void setRadius(double radius) {
			this.radius = radius;
		}

		public double getX() {
			return x;
		}

		public void setX(double x) {
			this.x = x;
		}

		public double getY() {
			return y;
		}

		public void setY(double y) {
			this.y = y;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Sensor [radius=");
			builder.append(radius);
			builder.append(", x=");
			builder.append(x);
			builder.append(", y=");
			builder.append(y);
			builder.append("]");
			return builder.toString();
		}

		public void set(double x, double y) {
			setX(x);
			setY(y);
			
		}

	}

	// /** Detect if there are any collisions in the StereoNode stored*/
	// public static final List<StereoNode<Node>>
	// detectCollisions(StereoNode<Node> snOne,StereoNode<Node> snTwo)
	// {
	// return null;
	// }

//	private static double x1, x2, y1, y2,
	private static double x, y, distance;

	public static final boolean areThereCollision(StereoNode<Node> snOne,
			StereoNode<Node> snTwo) {
		if (snOne == null || snTwo == null)
			throw new NullPointerException();
		final Sensor s1 = colliders.get(snOne);
		if (s1 == null)
			return false;
		final Sensor s2 = colliders.get(snTwo);
		if (s2 == null)
			return false;
		return areThereCollision(
		snOne.getLayoutX() + s1.getX(),
		snOne.getLayoutY() + s1.getY(),
		s1.getRadius(),
		 snTwo.getLayoutX() + s2.getX(),
		 snTwo.getLayoutY() + s2.getY(),
		s2.getRadius());
		
	}

	
	public static final boolean areThereCollision(double x1,double y1, double radius1, double x2, double y2, double radius2)
	{
		
		x = Math.abs(x1 - x2);
		y = Math.abs(y1 - y2);
		distance = Math.sqrt(x * x + y * y);
		if (radius1 +radius2 >= distance)
			return true;
		return false;
	}
	/** For optimisate the code, I only use one list that I clean each time */
	private static final List<StereoNode<Node>> collisionDetectedInstance = new LinkedList<>();

	public static  synchronized List<StereoNode<Node>> areThereCollision(
			StereoNode<Node> sno) {
		if (collisionDetectedInstance.size() > 0)
		collisionDetectedInstance.clear();
		if (sno!=null)
		for (StereoNode<Node> sn : colliders.keySet()) {
			if (sn != sno && areThereCollision(sno, sn)) {

				collisionDetectedInstance.add(sn);
			}
		}

		return collisionDetectedInstance;

	}

	
	public static Sensor remove(Object o) {
		
		return colliders.remove(o);
		
	}
	
}
