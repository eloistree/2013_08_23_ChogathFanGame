package be.vgqd.vrjam.game.chogath;

import javafx.scene.Group;
import javafx.stage.Stage;
import be.vgqd.vrjam.game.chogath.contract.impl.AdapterMode;
import be.vgqd.vrjam.game.chogath.contract.impl.Modes;
import be.vgqd.vrjam.game.chogath.contract.impl.X360OculusMode1;
import be.vgqd.vrjam.game.chogath.data.InGame;
import be.vgqd.vrjam.game.chogath.game.GUI;
import be.vgqd.vrjam.game.chogath.game.sounds.Sound;
import be.vgqd.vrjam.game.chogath.game.sounds.Sound.SoundType;
import be.vgqd.vrjam.game.chogath.scene.HomeScene;
import be.vgqd.vrjam.game.chogath.scene.IngameScene;

public class Game {

	private Stage stage;
	private SceneType currentSceneType;

	public enum SceneType {
		HOME, INGAME
	}


	private X360OculusMode1 controlMode = null;
	private static Game INSTANCE = null;

	public static Game createInstance(Stage s) {
		if (INSTANCE != null)
			throw new IllegalArgumentException("Singleton is already created");
		return INSTANCE = new Game(s);

	}

	public static Game getInstance() {
		return INSTANCE;
	}

	private Game(Stage s) {
		this.stage = s;
		init();
	}

	private void init() {

		controlMode = Modes.getContactMode1();
		controlMode.startRecordingData();

		Group g = new Group();
		GUI.store(SceneType.HOME, new HomeScene(g));
		g = new Group();
		GUI.store(SceneType.INGAME, new IngameScene(g));
		switchScene(SceneType.HOME);
		

	}

	public void start() {

		//GameData.setSound(0.5);
		switchScene(SceneType.INGAME);

		InGame.getInstance().start();
	}

	

	
	public void switchScene(SceneType type) {
		if (type != null) {
			currentSceneType= type;
			stage.setScene(GUI.get(type));
			if (type== SceneType.HOME){
				Sound.play("homeSound.mp3", SoundType.AMBIANCE);
				InGame.getInstance().exitTheGame();
			}
			else if(type== SceneType.INGAME){	
				Sound.play("homeSound.mp3", SoundType.AMBIANCE);
}
		}
	}

	public static AdapterMode getMode() {
		return INSTANCE.controlMode;
	}


	public SceneType getSceneType() {
		return currentSceneType;
	}

	public void exit() {
		save();
		System.exit(0);

	}

	private void save() {

	}

	
	public void pause() {
		InGame.getInstance().pause();

	}

}
