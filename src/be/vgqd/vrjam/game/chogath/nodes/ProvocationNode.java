package be.vgqd.vrjam.game.chogath.nodes;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.text.Text;
import be.vgqd.vrjam.game.chogath.data.modele.Boss;
import be.vgqd.vrjam.game.chogath.data.modele.Champion;

public class ProvocationNode extends Pane {

	private static double MAXWIDTH = 400;
	private static ImageViewBuilder<?> squareImageChampion = ImageViewBuilder
			.create().fitHeight(30).fitWidth(30).layoutX(4).layoutY(4);

	private ImageView imageView = squareImageChampion.build();
	private Text text = Texts.getTextbuilder().build();
	private Rectangle background = RectangleBuilder.create().fill(Color.BLACK)
			.stroke(Color.GREEN).arcWidth(10).arcHeight(10).width(MAXWIDTH)
			.height(40).build();

	private ProvocationNode(Champion champ, String msg) {
		if (msg==null)msg="";
		imageView = squareImageChampion.build();
		if(champ!=null)setChampion(champ);
		text.setWrappingWidth(MAXWIDTH - imageView.getFitWidth() - 10);
		text.setLayoutX(60);
		text.setLayoutY(text.getFont().getSize());

		setText(msg);
		getChildren().addAll(background, text, imageView);
	}

	public void setChampion(Champion champ) {

		background.setStroke(Color.GREEN);
		imageView.setImage(champ.getImage());
		imageView.setFitHeight(50);
		imageView.setFitWidth(50);
		imageView.setLayoutY(-10);
		text.setWrappingWidth(MAXWIDTH - imageView.getFitWidth()*2 - 15);
		text.setLayoutX( imageView.getFitWidth()+10);
		
	}
	
	public void setBoss(Boss boss) {
		Image img = boss.getChampion().getImageProfil();
		if (img==null){setChampion(boss.getChampion());return;}
		background.setStroke(Color.YELLOW);
		imageView.setImage(img);
		imageView.setFitHeight(100);
		imageView.setFitWidth(100);
		imageView.setLayoutY(-60);
		text.setWrappingWidth(MAXWIDTH - imageView.getFitWidth() - 15);
		text.setLayoutX( imageView.getFitWidth()+10);
	}

	public void setText(String msg) {

		text.setText(msg);
		setWidth();

	}

	public void setWidth() {
		double widthtext = text.getBoundsInLocal().getWidth();
		double widthImageText = 10 + imageView.getFitWidth() + widthtext;
		if (widthImageText > MAXWIDTH)
			widthImageText = MAXWIDTH;

		background.setWidth(widthImageText);

	}

	public static ProvocationNode getNode() {
		
		return new ProvocationNode(null, "");
	}
}
