package be.vgqd.vrjam.game.chogath.nodes;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.text.Text;

public class GameOverNode extends Pane {

	private TimeNode nTime;
	private RageQuitNode nRage;
	private KillNode nKill;
	private static final int w = 200, h = 100;

	private GameOverNode(long time, int rageQuit, int kill) {
		Text tGM = Texts.getTextbuilder().text("Game Over").build();
		nRage = RageQuitNode.getNode(false);
		nRage.setText(rageQuit);

		nTime = TimeNode.getNode();
		nTime.setText(time);

		nKill = KillNode.getNode();
		nKill.setText(kill);

		Rectangle rect = RectangleBuilder.create().fill(Color.BLACK)
				.stroke(Color.GREEN).arcWidth(50).arcHeight(50).width(w)
				.height(h).opacity(0.7).build();

		tGM.setFill(Color.RED);

		double height = Texts.getTextfont().getSize() + 4;
		tGM.setLayoutX(w / 2 - tGM.getBoundsInLocal().getWidth() / 2);
		tGM.setLayoutY(height * 1);

		nTime.setLayoutX(w / 2 - nTime.getBoundsInLocal().getWidth() / 2);
		nTime.setLayoutY(height * 2);

		nRage.setLayoutX(w / 2 - nRage.getBoundsInLocal().getWidth() / 2 + 30);
		nRage.setLayoutY(height * 3);

		nKill.setLayoutX(w / 2 - nKill.getBoundsInLocal().getWidth() / 2 - 30);
		nKill.setLayoutY(height * 3);

		getChildren().addAll(rect, tGM, nTime, nKill, nRage);

	}

	public static GameOverNode getNode(long time, int rageQuit, int kill) {

		return new GameOverNode(time, rageQuit, kill);
	}

	public void setText(long time, int rageQuit, int kill) {

		nTime.setText(time);
		nRage.setText(rageQuit);
		nKill.setText(kill);
		
	}

	public static GameOverNode getNode() {
		
		return getNode(0, 0, 0);
	}
}
