package be.vgqd.vrjam.game.chogath.nodes.link;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class LinkNode extends Pane{

	public LinkNode( final String url,Image img)
	{
		if(img==null)throw new NullPointerException();
		
		ImageView imgv = new ImageView(img);
		imgv.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				try {
					URI uri = new URI(url);
					java.awt.Desktop.getDesktop().browse(uri);
				} catch (URISyntaxException | IOException e) {
					e.printStackTrace();
				}
						
				
			}
		});
		getChildren().add(imgv);
		
		
	}
}
