package be.vgqd.vrjam.game.chogath.nodes;

import javafx.scene.Cursor;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextBuilder;

public class Texts {
	private static final javafx.scene.text.Font titleFont = new Font(
			"Arial", 25);
	private static final javafx.scene.text.Font textFont = new Font(
			"Arial", 15);


	private static final TextBuilder<?> titleBuilder = TextBuilder.create()
			.fill(Color.DARKRED).stroke(Color.RED).strokeWidth(0.1)
			.focusTraversable(true).cursor(Cursor.NONE).font(titleFont);
	private static final TextBuilder<?> textBuilder = TextBuilder.create()
			.fill(Color.GREEN).stroke(Color.LIGHTGREEN).strokeWidth(0.1)
			.focusTraversable(true).cursor(Cursor.NONE).font(textFont);
	public static javafx.scene.text.Font getTitlefont() {
		return titleFont;
	}
	public static javafx.scene.text.Font getTextfont() {
		return textFont;
	}
	public static TextBuilder<?> getTitlebuilder() {
		return titleBuilder;
	}
	public static TextBuilder<?> getTextbuilder() {
		return textBuilder;
	}

	
	
	
}
