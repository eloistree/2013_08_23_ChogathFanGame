package be.vgqd.vrjam.game.chogath.nodes;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class BossNode extends Pane {

	private static double imageWidth = 25, imageheight = 25;
	private int death = -1;
	private ImageView championImage = new ImageView();
	private Rectangle rectquit = new Rectangle();
	private Rectangle rectstate = new Rectangle();
	private int max;

	public BossNode(Image img,int value) {
		if (img == null)
			throw new IllegalArgumentException();
		rectquit.setHeight(imageheight);
		rectquit.setWidth(imageWidth);
		rectquit.setFill(Color.LIGHTGREEN);
		rectquit.setOpacity(0.7);
		rectquit.setVisible(false);

		rectstate.setHeight(imageheight);
		rectstate.setWidth(imageWidth);
		rectstate.setFill(Color.TRANSPARENT);

		championImage.setImage(img);
		championImage.setFitHeight(imageheight);
		championImage.setFitWidth(imageWidth);
		max= value;
		setText(value);
		Group g = new Group();
		g.getChildren().addAll(championImage, rectstate, rectquit);
		g.setLayoutX(-g.getBoundsInLocal().getWidth()/2);
		g.setLayoutY(-g.getBoundsInLocal().getHeight()/2);
		getChildren().add(g);
	}

	public void setText(int value) {
		death = value;
		rectquit.setVisible(false);
		if (death == -2) {
			rectquit.setVisible(true);
		} else if (death == -1) {
			this.setOpacity(0.4);

		} else {
			
			if (value > 6){
				double tmp = ((double)value - 5.0) / 3.0;
				rectstate.setStrokeWidth(tmp>1.0?1.0:tmp);
			}else
				rectstate.setStrokeWidth(0.5);
			this.setOpacity(1.0);
			switch (value) {

			case 0:
			case 1:
				rectstate.setStroke(Color.DARKRED);
				break;
			case 2:
				rectstate.setStroke(Color.RED);

				break;
			case 3:
				rectstate.setStroke(Color.ORANGE);
			case 4:
				rectstate.setStroke(Color.YELLOW);
				break;
			case 5:
				rectstate.setStroke(Color.LIGHTGREEN);

				break;
			case 6:
				default:
					int g,r,b;
					g =(int)(255.0*Color.DARKGREEN.getGreen()+ 255.0*(Color.LIGHTGREEN.getGreen()-Color.DARKGREEN.getGreen())*(1.0-(double)value/(double)max));
					r =(int)(255.0*Color.DARKGREEN.getRed() +255.0*(Color.LIGHTGREEN.getRed()-Color.DARKGREEN.getRed())*(1.0-(double)value/(double)max));
					b =(int)(255.0*Color.DARKGREEN.getBlue() +255.0*(Color.LIGHTGREEN.getBlue()-Color.DARKGREEN.getBlue())*(1.0-(double)value/(double)max));
				rectstate.setStroke(Color.rgb(r,g,b, 1.0));

			case -2:
			case -1:
				rectstate.setFill(Color.TRANSPARENT);
		break;
	
			}
		}

	}

	public static double getImageWidth() {
		return imageWidth;
	}

	public static void setImageWidth(double imageWidth) {
		BossNode.imageWidth = imageWidth;
	}

	public static double getImageHeight() {
		return imageheight;
	}

	public static void setImageHeight(double imageheight) {
		BossNode.imageheight = imageheight;
	}

	public int getDeath() {
		return death;
	}

	public void setImage(Image img) {
		championImage.setImage(img);

	}

	public Image getImage() {
		return championImage.getImage();
	}

	public static BossNode getNode(Image img, int value){return new BossNode(img,value);}
}
