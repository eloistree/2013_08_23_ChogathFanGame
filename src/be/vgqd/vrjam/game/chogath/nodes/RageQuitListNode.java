package be.vgqd.vrjam.game.chogath.nodes;

import java.util.HashMap;
import java.util.List;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import be.vgqd.vrjam.game.chogath.data.modele.Champion;

public class RageQuitListNode extends Pane {

	private HashMap<Champion, ChampionImageViewer> images = new HashMap<>();

	public RageQuitListNode() {
		Rectangle rect = new Rectangle();
		rect.setStroke(Color.LIGHTGREEN);
		rect.setFill(Color.BLACK);

		this.getChildren().add(rect);
		List<Champion> champions = Champion.getChampionsInstanceList();
		int l = (int) (Math.sqrt(champions.size()))+1 ;
		int h = l + 1;
		int padding= 5;
		ChampionImageViewer img = null;
		Champion champ = null;
		int cpt = 0;
		for (int i = 0; i < h; i++) {

			for (int j = 0; j < l; j++) {
				cpt = i*l  + j;
				if (cpt < champions.size()) {
					champ = champions.get(cpt);
					img = new ChampionImageViewer(champ.getImage());
					img.setLayoutX(j
							* (padding + ChampionImageViewer.getImageWidth()));
					img.setLayoutY(i
							* (padding + ChampionImageViewer.getImageHeight()));
					this.getChildren().add(img);
					images.put(champ, img);
				}
			}

		}
		rect.setWidth(this.getBoundsInLocal().getWidth());
		rect.setHeight(this.getBoundsInLocal().getHeight());
	}

	public ChampionImageViewer get(Champion champ) {
		return images.get(champ);
	}

	public static RageQuitListNode getNode() {
		return new RageQuitListNode();
	}

	public void setText(Champion champ, int value) {
		if (champ != null)
			get(champ).setText(value);

	}

}
