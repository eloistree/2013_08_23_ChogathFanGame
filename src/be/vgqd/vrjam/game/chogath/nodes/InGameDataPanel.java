package be.vgqd.vrjam.game.chogath.nodes;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public abstract class InGameDataPanel extends HBox{

	protected Text t;
	protected ImageView imagev;
	
	
	
	protected InGameDataPanel(Image img) {
		if (img==null)throw new NullPointerException();
		
		t=Texts.getTextbuilder().build();
		this.imagev= new ImageView(img);
		imagev.setFitWidth(t.getFont().getSize());imagev.setFitHeight(t.getFont().getSize());
		imagev.setLayoutY(-t.getBoundsInLocal().getHeight()*2/3);
		this.getChildren().addAll(t,imagev);
		setText(0);
	}


	public void setText(long value)
	{
	
		t.setText(""+value);
		imagev.setLayoutX(t.getBoundsInLocal().getWidth());
		
		
	}
	
	
}
